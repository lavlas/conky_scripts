#!/usr/bin/python -O

""" SOURCE:http://guildwarstemple.com/js/dragoncalcs.js
	All of this code was respectfully borrowed and 'pythonized' from guildwarstemple.com.
	NO EXPECTATION OF PYTHONIC CODE!!!!
"""

import urllib2
import math
import datetime
import re

###############################################################################
# CLASSES
###############################################################################
class GW2Event:
	def __init__(self, eName):
		self.eventName = eName
		self.statusTxt = ""
		self.timer = 0

	def getEventInfo(self):
		return "{}\n  {} - {}".format(self.eventName, str(self.timer), self.statusTxt)
###############################################################################
# END CLASSES
###############################################################################

###############################################################################
# FUNCTIONS
###############################################################################

"""
'dragon1info', 'Time Before Pre-Event', 'Pre-Event Began', 'Event(s) Outdated');
'dragon2info', 'Time Before 30 Min Window', '30 Min Spawning Window', 'Event(s) Outdated');
'dragon3info', 'Time Before 1 Hour 15 Minute Window', '1 Hour 15 Minute Spawning Window', 'Event(s) Outdated');
'dragon4info', 'Time Before 30 Min Window', '30 Min Spawning Window', 'Event(s) Outdated');
'dragon5info', 'Time Before 30 Min Window', '30 Min Spawning Window', 'Event(s) Outdated');
'dragon6info', 'Time Before 30 Min Window', '30 Min Spawning Window', 'Event(s) Outdated');
'dragon7info', '2 Hour Testing Time', '2 Hour Testing Time', 'Event(s) Outdated');
'dragon8info', '2 Hour Testing Time', '2 Hour Testing Time', 'Event(s) Outdated');
'dragon9info', '2 Hour Testing Time', '2 Hour Testing Time', 'Event(s) Outdated');
'dragon10info','Time Before Possible Activation', 'Event Can Be Activated', 'Event(s) Outdated');
"""
def eventStatusTxt(secs,  opening,  respawn,  mess1,  mess2,  mess3):
	eventMsg = ""

	if secs > 0:
		#Event is idle and the window for the event to start hasn't begun
		eventMsg = mess1.replace("Time Before ", "Until ")
	elif secs + opening > 0:
		#Event window is active
		eventMsg = mess2
	else:
		timeBehind = math.floor((secs /respawn) + 1)
		# All of the events use "Event(s) Outdated" for mess3 so I'm just shortening it
		eventMsg = "{}({})".format("Outdated", math.trunc(timeBehind))
	
	eventMsg = eventMsg.replace(" Hour ", "Hr ");
	eventMsg = eventMsg.replace(" Minute ", "Min ");
	return "{}".format(eventMsg)


def eventCountDown(secs,  respawn,  eventWindow):
	secsBehind = False
	currentEventGap = False
	timeBehind = 0
	resetTime = respawn

	if secs <= 0:
		secsBehind = True;
		secs = math.fabs(secs)
		#The guildwarstemple negates secs then tests to see if it is within the event window so I just take the absolute value of it since we already know it is less than 0
		if secs <= eventWindow:
				secs = eventWindow - secs;
				currentEventGap = True
		else:
			#This section of code doesn't make sense.  It seems to ultimately set secs = reset time
			timeBehind = math.floor(float(secs) / respawn)
			secs -= timeBehind * resetTime
			secs = resetTime - secs

			currentEventGap = False
	else:
		currentEventGap = False

	return datetime.timedelta(seconds=secs)


###############################################################################
# END FUNCTIONS
###############################################################################

maguuma_timer_url = 'http://guildwarstemple.com/dragontimer/events.php?serverKey=117&langKey=en'

#Separates the events into different categories for 'Dragons' and Low/High Level.  You can iterate through this to organize the events or only choose specific event types
eventTypes = {"Dragon Events":["dragon1", "dragon2", "dragon3"], "Low Level Events":["dragon10", "dragon4", "dragon5", "dragon11"], "High Level Events":["dragon6", "dragon7", "dragon8", "dragon9"]}

jsCountDownFuncNames = "zxcCountDown", "zxcInfo"
jsEventDict = {"dragon1":GW2Event("The Shatterer"),  
	"dragon2":GW2Event("Tequatl the Sunless"),
	"dragon3":GW2Event("Claw of Jormag"), 
	"dragon4":GW2Event("Shadow Behemeth"), "dragon5":GW2Event("Fire Elemental"),
	"dragon6":GW2Event("Megadestroyer"),"dragon7":GW2Event("Temple of Balthazar"),
	"dragon8":GW2Event("Temp of Grenth"),"dragon9":GW2Event("Temple of Lyssa"), 
	"dragon10":GW2Event("The Frozen Maw"), 
	"dragon11":GW2Event("Great Jungle Wurm")
	}


for line in urllib2.urlopen(maguuma_timer_url):
	for funcName in jsCountDownFuncNames:
		if funcName in line:
			if __debug__: print line.strip()
			if funcName == "zxcInfo":
				 #regExResult = re.search(r"\s*({})\('(dragon\d+info)',\s*(-?\d+),\s*(-?\d+),\s*(-?\d+),\s+.*\)".format(funcName), line)
				 regExResult = re.search(r"\s*{}\('(dragon\d+)info',\s*(-?\d+),\s*(-?\d+),\s*(-?\d+),\s*'([^']+)',\s*'([^']+)',\s*'([^']+)',*.*\)".format(funcName), line)
				 if __debug__: print "-->{}".format(regExResult.groups())
				 if jsEventDict.get(regExResult.group(1)) != None:
					 preEventMsg = jsEventDict[regExResult.group(1)].eventName
					 secs = int(regExResult.group(2))
					 opening = int(regExResult.group(3))
					 respawn = int(regExResult.group(4))
					 mess1 = regExResult.group(5)
					 mess2 = regExResult.group(6)
					 mess3 = regExResult.group(7)
					 #print "{}\n {}".format(preEventMsg, eventStatusTxt(secs,  opening,  respawn, mess1, mess2, mess3)),  
					 jsEventDict[regExResult.group(1)].statusTxt = eventStatusTxt(secs,  opening,  respawn, mess1, mess2, mess3)  
			elif funcName == "zxcCountDown":
				regExResult = re.search(r"\s*{}\('(dragon\d+)timer', \s*'Refreshing',\s*(-?\d+),\s*(-?\d+),\s*(-?\d+).*\);?".format(funcName), line)
				if __debug__: print "-->{}".format(regExResult.groups())
				if jsEventDict.get(regExResult.group(1)) != None:
					preEventMsg = jsEventDict[regExResult.group(1)].eventName
					secs = int(regExResult.group(2))
					respawn	= int(regExResult.group(3))
					eventWindow = int(regExResult.group(4))
					#print "{} - {}".format(preEventMsg, eventCountDown(secs, respawn, eventWindow))
					jsEventDict[regExResult.group(1)].timer = eventCountDown(secs, respawn, eventWindow)

if __debug__:
#Print all of the events
	for eventGroup in eventTypes.keys():
		print eventGroup
		for event in eventTypes[eventGroup]:
			print jsEventDict[event].getEventInfo()
else:
#Print only the Dragon events by default
	for event in eventTypes["Dragon Events"]:
		print jsEventDict[event].getEventInfo()


